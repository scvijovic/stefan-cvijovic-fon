<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
	<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
	<div class="navbar-nav">
	<span class="navbar-text">STUDENT SERVICE</span>	
	<ul class="nav nav-pills">
	
		<li class="nav-item dropdown">
    	<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Subject</a>
    	<div class="dropdown-menu">
    	<c:url value="/subject/subject_add" var="subject_add"></c:url>
		<a href="<c:out value="${subject_add}"/>" class="dropdown-item" class="nav-item nav-link active">Add subject</a>
		<div class="dropdown-divider"></div>
		<c:url value="/subject/subject_all" var="subject_all"></c:url>
		<a href="<c:out value="${subject_all}"/>" class="dropdown-item" class="nav-item nav-link active">All subjects</a>
    	</div>
  		</li>
  		
  		<li class="nav-item dropdown">
    	<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Professor</a>
    	<div class="dropdown-menu">
    	<c:url value="/professor/professor_add" var="professor_add"></c:url>
		<a href="<c:out value="${professor_add}"/>" class="dropdown-item" class="nav-item nav-link active">Add professor</a>
		<div class="dropdown-divider"></div>
		<c:url value="/professor/professor_all" var="professor_all"></c:url>
		<a href="<c:out value="${professor_all}"/>" class="dropdown-item" class="nav-item nav-link active">All professors</a>
    	</div>
  		</li>
  		
  		<li class="nav-item dropdown">
    	<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Student</a>
    	<div class="dropdown-menu">
    	<c:url value="/student/student_add" var="student_add"></c:url>
		<a href="<c:out value="${student_add}"/>" class="dropdown-item" class="nav-item nav-link active">Add student</a>
		<div class="dropdown-divider"></div>
		<c:url value="/student/student_all" var="student_all"></c:url>
		<a href="<c:out value="${student_all}"/>" class="dropdown-item" class="nav-item nav-link active">All students</a>
    	</div>
  		</li>
  		
  		<li class="nav-item dropdown">
    	<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Exam</a>
    	<div class="dropdown-menu">
    	<c:url value="/exam/exam_add" var="exam_add"></c:url>
		<a href="<c:out value="${exam_add}"/>" class="dropdown-item" class="nav-item nav-link active">Add exam</a>
		<div class="dropdown-divider"></div>
		<c:url value="/exam/exam_all" var="exam_all"></c:url>
		<a href="<c:out value="${exam_all}"/>" class="dropdown-item" class="nav-item nav-link active">All exams</a>
    	</div>
  		</li>
  		
  		<li class="nav-item dropdown">
    	<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Exam registration</a>
    	<div class="dropdown-menu">
    	<c:url value="/examregistration/examregistration_add" var="examregistration_add"></c:url>
		<a href="<c:out value="${examregistration_add}"/>" class="dropdown-item" class="nav-item nav-link active">Add exam registration</a>
		<div class="dropdown-divider"></div>
		<c:url value="/examregistration/examregistration_all" var="examregistration_all"></c:url>
		<a href="<c:out value="${examregistration_all}"/>" class="dropdown-item" class="nav-item nav-link active">All exam registrations</a>
    	</div>
  		</li>
  		
  	</ul>
	</div>
	</div>
	<c:url value="/authentication/logout" var="logout"></c:url>
	<span class="navbar-text">Username: ${sessionScope.user.username}</span><a class="nav-link mr-sm-2" href="${logout}"> Logout</a>
	</nav>
	
	
</body>
</html>
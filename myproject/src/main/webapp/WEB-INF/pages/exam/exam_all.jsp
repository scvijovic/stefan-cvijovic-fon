<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>Subject</th>
				<th>Professor</th>
				<th>Date of Exam</th>
				<th>Details</th>
				<th>Edit</th>
				<th>Remove</th>
			</tr>
			
			<c:forEach items="${exams}" var="exam">

				<c:url value="/exam/exam_remove" var="examRemove">
					<c:param name="id" value="${exam.id}"></c:param>
				</c:url>
				<c:url value="/exam/exam_edit" var="examEdit">
					<c:param name="id" value="${exam.id}"></c:param>
				</c:url>
				<c:url value="/exam/exam_details" var="examDetails">
					<c:param name="id" value="${exam.id}"></c:param>
				</c:url>
				<tr>
					<td>${exam.subject.name}</td>
					<td>${exam.professor.fullname}</td>
					<td>${exam.dateOfExam}</td>
					<td><a href="${examDetails}">Details</a></td>
					<td><a href="${examEdit}">Edit</a></td>
					<td><a href="${examRemove}"
						onclick="return confirmDelete()">Remove</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<script type="text/javascript">
	function confirmDelete(){
		var agree = confirm("By confirming you'll remove this exam");
		  if(agree == true){
		    return true
		}
		else{
		return false;
		}
		}
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>

</head>
<body>
	<div class="container">
		<span class="navbar-text">THIS IS EXAM ADD PAGE</span>
		<form:form action="${pageContext.request.contextPath}/exam/exam_save"
			method="post" modelAttribute="examDto">
			
			<div class="row">
				<div class="form-group col-xs-3">
					Subject:
					<form:select path="subject" class="form-control">
						<form:options items="${subjects}" itemValue="name"
							itemLabel="name" />
					</form:select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-3">
					Date of exam:
					<form:input type="date" path="dateOfExam" id="dateId" class="form-control" />
					<div>
						<form:errors path="dateOfExam" />
					</div>
			</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-3">
					Professor:
					<form:select path="professor" class="form-control">
						<form:options items="${professors}" itemValue="id"
							itemLabel="fullname" />
					</form:select>
					</div>
			</div>

					<button id="save" class="btn btn-primary">Save</button>
		</form:form>
	</div>
</body>
</html>
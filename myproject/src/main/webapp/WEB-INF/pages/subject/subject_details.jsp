<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Semester</th>
				<th>Year of study</th>
			</tr>
			<tr>
				<td>${subjectDto.name}</td>
				<td>${subjectDto.description}</td>
				<td>${subjectDto.semester}</td>
				<td>${subjectDto.yearOfStudy}</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
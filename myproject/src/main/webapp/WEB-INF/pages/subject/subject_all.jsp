<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
		<table class="table table-striped">
<tbody>
	<tr>
		<th>Name</th>
		<th>Description</th>
    	<th>Semester </th>
    	<th>Year of study</th>
    	<th>Details</th>
    	<th>Edit</th>
    	<th>Remove</th>
	</tr>
	<c:forEach items="${subjects}" var="subject">
		
		<c:url value="/subject/subject_remove"  var="subjectRemove">
		<c:param name="name" value="${subject.name}"></c:param>
		</c:url>
		<c:url value="/subject/subject_edit" var="subjectEdit">
		<c:param name="name" value="${subject.name}"></c:param>
		</c:url>
		<c:url value="/subject/subject_details" var="subjectDetails">
		<c:param name="name" value="${subject.name}"></c:param>
		</c:url>
    	<tr>
    	<td>${subject.name}</td>
    	<td>${subject.description}</td>
    	<td>${subject.semester} </td>
    	<td>${subject.yearOfStudy} </td>
    	<td><a href="${subjectDetails}">Details</a> </td>
    	<td><a href="${subjectEdit}">Edit</a> </td>
    	<td><a href="${subjectRemove}" onclick="return confirmDelete()">Remove</a> </td>
    	 </tr>
</c:forEach>
</tbody>
</table>
<script type="text/javascript">
	function confirmDelete(){
		var agree = confirm("By confirming you'll remove this subject");
		  if(agree == true){
		    return true
		}
		else{
		return false;
		}
		}
	</script>
</body>
</html>
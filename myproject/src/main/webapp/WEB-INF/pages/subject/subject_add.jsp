<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
<style type="text/css">
.error{
	color: red;
}
</style>
</head>
<body>
		<div class="container" > 
		<span class="navbar-text">THIS IS SUBJECT ADD PAGE</span>
		<form:form action="${pageContext.request.contextPath}/subject/subject_save" method="post" modelAttribute="subjectDto">
		<div class="row">
		<div class="form-group col-xs-3">
		Name:<form:input type="text" path="name" id="nameId" class="form-control"/>
		<form:errors path="name" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Description:<form:input type="text" path="description" id="descriptionId" class="form-control"/>
		<form:errors path="description" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Year of study:<form:input type="text" path="yearOfStudy" id="yearOfStudyId" class="form-control"/>
		<form:errors path="yearOfStudy" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		<form:label path="semester">Semester</form:label>
					
					<form:select path="semester" class="form-control">
						<form:option value="" label="Choose Semester"></form:option>
						<form:options items="${semesters}" itemValue="name" itemLabel="name"/>									
						</form:select>
						
					 <form:errors path="semester" cssClass="error" />
					 
		</div>
		</div>
		<button id="save" class="btn btn-primary">Save</button> 
	</form:form>
	</div>
</body>
</html>
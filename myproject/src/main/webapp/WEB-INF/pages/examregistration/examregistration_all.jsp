<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>Subject</th>
				<th>Professor</th>
				<th>Student</th>
				<th>Date of exam</th>
			</tr>
			<c:forEach items="${examRegistrations}" var="examRegistration">
				<tr>
					<td>${examRegistration.exam.subject.name}</td>
					<td>${examRegistration.exam.professor.fullname}</td>
					<td>${examRegistration.student.fullname}</td>
					<td>${examRegistration.exam.dateOfExam}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>
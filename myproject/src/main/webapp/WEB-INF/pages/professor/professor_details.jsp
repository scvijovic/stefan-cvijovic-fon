<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Address</th>
				<th>City</th>
				<th>Phone</th>
				<th>Re-election date</th>
				<th>Title</th>
			</tr>
			<tr>
				<td>${professorDto.firstname}</td>
				<td>${professorDto.lastname}</td>
				<td>${professorDto.email}</td>
				<td>${professorDto.adress}</td>
				<td>${professorDto.city.name}</td>
				<td>${professorDto.phone}</td>
				<td>${professorDto.reelectionDate}</td>
				<td>${professorDto.title.name}</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
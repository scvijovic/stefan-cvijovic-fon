<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
		<div class="container" >
		<span class="navbar-text">THIS IS PROFESSOR EDIT PAGE</span>
		<form:form action="${pageContext.request.contextPath}/professor/professor_update"
		 method="post" modelAttribute="professorDto">
		<div class="row">
		<div class="form-group col-xs-3">
		First Name:<form:input type="text" path="firstname" id="firstnameId" class="form-control"/>
		<form:errors path="firstname" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Last Name:<form:input type="text" path="lastname" id="lastnameId" class="form-control"/>
		<form:errors path="lastname" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Email:<form:input type="text" path="email" id="emailId" class="form-control"/>
		<form:errors path="email" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Address:<form:input type="text" path="adress" id="adressId" class="form-control"/>
		<form:errors path="adress" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		City:
		<form:select path="city" class="form-control">
			<form:options items="${cities}" itemValue="id" itemLabel="name"/>		
		</form:select>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Phone:<form:input type="text" path="phone" id="phoneId" class="form-control"/>
		<form:errors path="phone" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Re-election date:<form:input type="date" path="reelectionDate" id="reelectionDateId" class="form-control"/>
		<form:errors path="reelectionDate" cssClass="error"/>
		</div>
		</div>
		<div class="row">
		<div class="form-group col-xs-3">
		Title:
		<form:select path="title" class="form-control">
			<form:options items="${titles}" itemValue="id" itemLabel="name"/>		
		</form:select>
		</div>
		</div>
			
		<button id="save" class="btn btn-primary">Save</button> 
	</form:form>
	</div>
</body>
</html>
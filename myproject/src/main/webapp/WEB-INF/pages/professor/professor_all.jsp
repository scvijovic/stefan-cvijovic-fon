<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Address</th>
				<th>City</th>
				<th>Phone</th>
				<th>Re-election date</th>
				<th>Title</th>
				<th>Details</th>
				<th>Edit</th>
				<th>Remove</th>
			</tr>
			<c:forEach items="${professors}" var="professor">

				<c:url value="/professor/professor_remove" var="professorRemove">
					<c:param name="id" value="${professor.id}"></c:param>
				</c:url>
				<c:url value="/professor/professor_edit" var="professorEdit">
					<c:param name="id" value="${professor.id}"></c:param>
				</c:url>
				<c:url value="/professor/professor_details" var="professorDetails">
					<c:param name="id" value="${professor.id}"></c:param>
				</c:url>
				<tr>
					<td>${professor.firstname}</td>
					<td>${professor.lastname}</td>
					<td>${professor.email}</td>
					<td>${professor.adress}</td>
					<td>${professor.city.name}</td>
					<td>${professor.phone}</td>
					<td>${professor.reelectionDate}</td>
					<td>${professor.title.name}</td>
					<td><a href="${professorDetails}">Details</a></td>
					<td><a href="${professorEdit}">Edit</a></td>
					<td><a href="${professorRemove}"
						onclick="return confirmDelete()">Remove</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<script type="text/javascript">
	function confirmDelete(){
		var agree = confirm("By confirming you'll remove this professor");
		  if(agree == true){
		    return true
		}
		else{
		return false;
		}
		}
	</script>
</body>
</html>
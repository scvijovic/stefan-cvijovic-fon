<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
		THIS IS PROFESSOR HOME PAGE
		<div>
		<c:url value="/professor/professor_add" var="professor_add"></c:url>
		<a href="<c:out value="${professor_add}"/>">Add professor</a>
		</div>
		<div>
		<c:url value="/professor/professor_all" var="professor_all"></c:url>
		<a href="<c:out value="${professor_all}"/>">All professor</a>
		</div>
</body>
</html>
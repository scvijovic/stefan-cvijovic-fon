<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>Index number</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Adress</th>
				<th>City</th>
				<th>Phone</th>
				<th>Current year</th>
				<th>Details</th>
				<th>Edit</th>
				<th>Remove</th>
			</tr>
			<c:forEach items="${students}" var="student">

				<c:url value="/student/student_remove" var="studentRemove">
					<c:param name="id" value="${student.id}"></c:param>
				</c:url>
				<c:url value="/student/student_edit" var="studentEdit">
					<c:param name="id" value="${student.id}"></c:param>
				</c:url>
				<c:url value="/student/student_details" var="studentDetails">
					<c:param name="id" value="${student.id}"></c:param>
				</c:url>
				<tr>
					<td>${student.indexNumber}</td>
					<td>${student.firstname}</td>
					<td>${student.lastname}</td>
					<td>${student.email}</td>
					<td>${student.adress}</td>
					<td>${student.city.name}</td>
					<td>${student.phone}</td>
					<td>${student.currentYearOfStudy}</td>
					<td><a href="${studentDetails}">Details</a></td>
					<td><a href="${studentEdit}">Edit</a></td>
					<td><a href="${studentRemove}"
						onclick="return confirmDelete()">Remove</a>
						
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<script type="text/javascript">
	
	
	function confirmDelete(){
		var agree = confirm("By confirming you'll remove this student");
		  if(agree == true){
		    return true
		}
		else{
		return false;
		}
		}
	
	
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>
	<table class="table table-striped">
<tbody>
	<tr>
			<th>Index number</th>
		<th>First name</th>
    	<th>Last name </th>
    	<th>Email</th>
    	<th>Address</th>
    	<th>City</th>
    	<th>Phone</th>
    	<th>Current year</th>
	</tr>
	<tr>
    	<td>${studentDto.indexNumber}</td>
    	<td>${studentDto.firstname}</td>
    	<td>${studentDto.lastname} </td>
    	<td>${studentDto.email} </td>
    	<td>${studentDto.adress}</td>
    	<td>${studentDto.city.name}</td>
    	<td>${studentDto.phone}</td>
    	<td>${studentDto.currentYearOfStudy}</td>
    	</tr>
    	</tbody>
    	</table>
</body>
</html>
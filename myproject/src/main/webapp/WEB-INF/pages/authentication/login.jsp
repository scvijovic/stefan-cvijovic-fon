<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>		
		<div class="col-xl-3 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
		<form:form action="${pageContext.request.contextPath}/authentication/login"
		 method="post" modelAttribute="userDto" class="text-center border border-light p-5">
		 <p class="h4 mb-4">Welcome to Student Service Login Page</p>
			Username:
			<form:input type="username" path="username" id="username" class="form-control mb-4" />
			Password:
			<form:input type="password"  id="password" path="password" class="form-control mb-4"/>
		  
			<button id="save" class="btn btn-info btn-block my-4">Login</button> 

		</form:form>
		</div>
</body>
</html>
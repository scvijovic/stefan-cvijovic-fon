package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.model.TitleDto;
import stefan.cvijovic.fon.myproject.service.TitleService;

@Component
public class TitleDtoFormatter implements Formatter<TitleDto> {
	
	private final TitleService titleService;
	
	@Autowired
	public TitleDtoFormatter(TitleService titleService) {
		super();
		this.titleService = titleService;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {
		return titleDto.getId().toString();
	}

	@Override
	public TitleDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		TitleDto titleDto = titleService.findById(id);
		return titleDto;
	}

}

package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.TitleConverter;
import stefan.cvijovic.fon.myproject.entity.TitleEntity;
import stefan.cvijovic.fon.myproject.model.TitleDto;
import stefan.cvijovic.fon.myproject.repository.TitleRepository;
import stefan.cvijovic.fon.myproject.service.TitleService;
@Service
@Transactional
public class TitleServiceImpl implements TitleService {
	
	private TitleRepository titleRepository;
	private TitleConverter titleConverter;
	
	@Autowired
	public TitleServiceImpl(TitleRepository titleRepository, TitleConverter titleConverter) {
		super();
		this.titleRepository = titleRepository;
		this.titleConverter = titleConverter;
	}

	@Override
	public void save(TitleDto titleDto) {
		titleRepository.save(titleConverter.dtoToEntity(titleDto));
		
	}

	@Override
	public List<TitleDto> getAll() {
		List<TitleEntity> entityList = titleRepository.findAll();
		List<TitleDto> dtoList = titleConverter.entityToDtoList(entityList);
		return dtoList;
	}

	@Override
	public TitleDto findById(Long id) {
		Optional<TitleEntity> titleOptional = titleRepository.findById(id);
		if(titleOptional.isPresent()) {
			return titleConverter.entityToDto(titleOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		titleRepository.deleteById(id);
		
	}

}

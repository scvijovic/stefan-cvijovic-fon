package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.CityDto;

public interface CityService {
	
		public void save(CityDto cityDto);
		public List<CityDto> getAll();
		public CityDto findById(Long id);
		public void deleteById(Long id);
}

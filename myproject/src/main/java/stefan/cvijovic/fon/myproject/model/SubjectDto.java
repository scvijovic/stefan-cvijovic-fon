package stefan.cvijovic.fon.myproject.model;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import stefan.cvijovic.fon.myproject.entity.Semester;

public class SubjectDto implements Serializable {
	public static final long serialVersionUID = 149040032020L;
	@NotEmpty(message = "You must enter name")
	private String name;
	private String description;
	private int yearOfStudy;
	private Semester semester;
	
	public SubjectDto() {
		//
	}

	public SubjectDto(String name, String description, int yearOfStudy, Semester semester) {
		super();
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}
}

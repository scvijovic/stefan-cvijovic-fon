package stefan.cvijovic.fon.myproject.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ProfessorDto implements Serializable {
	private static final long serialVersionUID = 124504032020L;
	//@NotNull(message = "You must enter professor id")
	private Long id;
	@NotEmpty(message = "You must enter first name")
	private String firstname;
	@NotEmpty(message = "You must enter last name")
	private String lastname;
	private String email;
	private String adress;
	private CityDto city;
	private String phone;
	@NotNull(message = "You must choose reelection date")
	private Date reelectionDate;
	@NotNull(message = "You must choose title")
	private TitleDto title;

	public ProfessorDto() {
		//city = new CityDto();
		//title = new TitleDto();
	}

	public ProfessorDto(Long id, String firstname, String lastname, String email, String adress, CityDto city,
			String phone, Date reelectionDate, TitleDto title) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleDto getTitle() {
		return title;
	}

	public void setTitle(TitleDto title) {
		this.title = title;
	}
	
	public String getFullname() {
		return firstname + " " + lastname;
	}

}

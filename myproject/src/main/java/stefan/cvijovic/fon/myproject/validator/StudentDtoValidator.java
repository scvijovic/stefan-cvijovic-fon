package stefan.cvijovic.fon.myproject.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import stefan.cvijovic.fon.myproject.model.StudentDto;
import stefan.cvijovic.fon.myproject.service.StudentService;

public class StudentDtoValidator implements Validator {
	
	private final StudentService studentService;
	
	@Autowired
	public StudentDtoValidator(StudentService studentService) {
		super();
		this.studentService = studentService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.equals(clazz);
	}
	
	

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto)target;
		
		/*if(studentDto.getIndexNumber()!=null) {
			if(studentDto.getIndexNumber().trim().length() < 10) {
				errors.rejectValue("indexNumber", "StudentDto.indexNumber",
						"Index number must have more than 10 characters");
			}
		}
		*/
		
		if(studentDto.getFirstname()!=null) {
			if(studentDto.getFirstname().trim().length() < 3) {
				errors.rejectValue("firstname", "StudentDto.firstname",
						"Firstname must have more than 3 characters");
			}
		}
		
		if(studentDto.getLastname()!=null) {
			if(studentDto.getLastname().trim().length() < 3) {
				errors.rejectValue("lastname", "StudentDto.lastname",
						"Lastname must have more than 3 characters");
			}
		}
		
		if(studentDto.getEmail()!=null) {
			if(!studentDto.getEmail().contains("@")) {
				errors.rejectValue("email", "StudentDto.email", 
						"Email must contain @");
			}
		}
		
		if(studentDto.getAdress()!=null) {
			if(studentDto.getAdress().trim().length() < 3) {
				errors.rejectValue("adress", "StudentDto.adress",
						"Address must have more than 3 characters");
			}
		}
		
		if(studentDto.getPhone()!=null) {
			if(studentDto.getPhone().trim().length() < 6) {
				errors.rejectValue("phone", "StudentDto.phone",
						"Phone number must have more than 6 characters");
			}
		}
		
		
		
		
		
	}

}

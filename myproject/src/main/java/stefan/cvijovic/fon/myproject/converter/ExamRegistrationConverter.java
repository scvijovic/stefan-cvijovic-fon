package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.ExamRegistrationEntity;
import stefan.cvijovic.fon.myproject.model.ExamRegistrationDto;

@Component
public class ExamRegistrationConverter {
	
	private final ExamConverter examConverter;
	private final StudentConverter studentConverter;
	
	@Autowired
	public ExamRegistrationConverter(@Lazy ExamConverter examConverter, @Lazy StudentConverter studentConverter) {
		super();
		this.examConverter = examConverter;
		this.studentConverter = studentConverter;
	}
	
	
	public ExamRegistrationDto entityToDto(ExamRegistrationEntity examRegistrationEntity) {
		return new ExamRegistrationDto(examRegistrationEntity.getId(), examConverter.entityToDto(examRegistrationEntity.getExam()), 
				studentConverter.entityToDto(examRegistrationEntity.getStudent()));
	}
	
	public ExamRegistrationEntity dtoToEntity(ExamRegistrationDto examRegistrationDto) {
		return new ExamRegistrationEntity(examRegistrationDto.getId(), examConverter.dtoToEntity(examRegistrationDto.getExam()),
				studentConverter.dtoToEntity(examRegistrationDto.getStudent()));
	}
	
	public List<ExamRegistrationEntity> dtoToEntityList(List<ExamRegistrationDto> dtoList) {
		List<ExamRegistrationEntity> entityList = new ArrayList<ExamRegistrationEntity>();
		for(ExamRegistrationDto examRegistrationDto: dtoList) {
			entityList.add(dtoToEntity(examRegistrationDto));
		}
		return entityList;
	}
	
	public List<ExamRegistrationDto> entityToDtoList(List<ExamRegistrationEntity> entityList) {
		List<ExamRegistrationDto> dtolist = new ArrayList<ExamRegistrationDto>();
		for(ExamRegistrationEntity examRegistrationEntity: entityList) {
			dtolist.add(entityToDto(examRegistrationEntity));
		}
		return dtolist;
	}
	
	
	
	
	

}

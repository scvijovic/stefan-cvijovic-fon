package stefan.cvijovic.fon.myproject.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class CityEntity implements Serializable {
	public static final long serialVersionUID = 14704022020L;
	@Id
	@Column(name = "CITY_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true)
	private int number;
	@Column(length = 30)
	private String name;
	
	public CityEntity() {
		super(); //
		// TODO Auto-generated constructor stub
	}

	public CityEntity(Long id, int number, String name) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	
	
	
	
	
	
	
	
	
}

package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.model.ProfessorDto;
import stefan.cvijovic.fon.myproject.service.ProfessorService;

@Component
public class ProfessorDtoFormatter implements Formatter<ProfessorDto> {
	
	private final ProfessorService professorService;
	
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService) {
		super();
		this.professorService = professorService;
	}

	@Override
	public String print(ProfessorDto professorDto, Locale locale) {
		return professorDto.getId().toString();
	}

	@Override
	public ProfessorDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ProfessorDto professorDto = professorService.findById(id);
		return professorDto;
	}
	
}

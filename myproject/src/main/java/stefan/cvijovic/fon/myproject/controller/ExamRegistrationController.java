package stefan.cvijovic.fon.myproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.cvijovic.fon.myproject.model.ExamDto;
import stefan.cvijovic.fon.myproject.model.ExamRegistrationDto;
import stefan.cvijovic.fon.myproject.model.StudentDto;
import stefan.cvijovic.fon.myproject.service.ExamRegistrationService;
import stefan.cvijovic.fon.myproject.service.ExamService;
import stefan.cvijovic.fon.myproject.service.StudentService;

@Controller
@SessionAttributes(value = "examRegistrationDto")
public class ExamRegistrationController {
	
	private final ExamRegistrationService examRegistrationService;
	private final ExamService examService;
	private final StudentService studentService;
	
	@Autowired
	public ExamRegistrationController(ExamRegistrationService examRegistrationService, ExamService examService,
			StudentService studentService) {
		super();
		this.examRegistrationService = examRegistrationService;
		this.examService = examService;
		this.studentService = studentService;
	}
	
	@GetMapping(value = "examregistration/examregistration_add")
	public ModelAndView toPage() {
		ModelAndView modelAndView = new ModelAndView("examregistration/examregistration_add");
		return modelAndView;
	}
	
	@GetMapping(value = "examregistration/examregistration_all")
	public ModelAndView all(){
		ModelAndView modelAndView = new ModelAndView("examregistration/examregistration_all");
		return modelAndView;
	}
	
	@GetMapping(value = "examregistration/examregistration_remove")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/examregistration/examregistration_all");
		examRegistrationService.deleteById(id);
		return modelAndView;
	}
	
	@PostMapping(value = "examregistration/examregistration_save")
	public ModelAndView save(@Valid @ModelAttribute("examRegistrationDto") ExamRegistrationDto examRegistrationDto
			, Errors errors, SessionStatus sessionStatus ) {  
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(examRegistrationDto);
		if(errors.hasErrors()) {
			modelAndView.setViewName("/examregistration/examregistration_add");
			modelAndView.addObject("examRegistrationDto", examRegistrationDto);
		} else {
			examRegistrationService.save(examRegistrationDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/examregistration/examregistration_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "examregistration/examregistration_edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("examregistration/examregistration_edit");
		modelAndView.addObject("examRegistrationDto", examRegistrationService.findById(id));
		return modelAndView;
	}
	
	
	@PostMapping(value = "examregistration/examregistration_update")
	public ModelAndView update(@Valid @ModelAttribute("examRegistrationDto") ExamRegistrationDto examRegistrationDto
			, Errors errors, SessionStatus sessionStatus ) {  
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(examRegistrationDto);
		if(errors.hasErrors()) {
			modelAndView.setViewName("/examregistration/examregistration_edit");
			modelAndView.addObject("examRegistrationDto", examRegistrationDto);
		} else {
			examRegistrationService.save(examRegistrationDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/examregistration/examregistration_all");
		}
		return modelAndView;
	}
	
	
	@GetMapping(value = "examregistration/examregistration_details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("examregistration/examregistration_details");
		modelAndView.addObject("examRegistrationDto", examRegistrationService.findById(id));
		return modelAndView;
	}
	
	@ModelAttribute(name="examRegistrationDto")
	public ExamRegistrationDto examDto() {
		return new ExamRegistrationDto();
	}
	
	
	@ModelAttribute(name = "exams")
	public List<ExamDto> exams(){
		return examService.getAll();
	}
	
	@ModelAttribute(name = "students")
	public List<StudentDto> students(){
		return studentService.getAll();
	}
	
	@ModelAttribute(name = "examRegistrations")
	public List<ExamRegistrationDto> examregs(){
		return examRegistrationService.getAll();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	
	
	
	
	
	
	
	
	

}

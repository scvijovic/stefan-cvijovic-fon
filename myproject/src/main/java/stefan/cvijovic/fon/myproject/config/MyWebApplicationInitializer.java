package stefan.cvijovic.fon.myproject.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;



public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	public MyWebApplicationInitializer() {
		System.out.println("=================================================================");
		System.out.println("==================== MyWebApplicationInitializer =========================");
		System.out.println("=================================================================");
	}
	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {MyDatabaseConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {MyWebAppContextConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}

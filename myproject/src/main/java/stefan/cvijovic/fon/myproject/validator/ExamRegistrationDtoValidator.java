package stefan.cvijovic.fon.myproject.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import stefan.cvijovic.fon.myproject.model.ExamRegistrationDto;
import stefan.cvijovic.fon.myproject.service.ExamRegistrationService;

public class ExamRegistrationDtoValidator implements Validator {
	
	private final ExamRegistrationService examRegistrationService;
	
	@Autowired
	public ExamRegistrationDtoValidator(ExamRegistrationService examRegistrationService) {
		super();
		this.examRegistrationService = examRegistrationService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return ExamRegistrationDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ExamRegistrationDto examRegistrationDto = (ExamRegistrationDto)target;
		
		if(examRegistrationDto.getStudent().getCurrentYearOfStudy() < examRegistrationDto.getExam().getSubject().getYearOfStudy()) {
			errors.rejectValue("studentDto", "ExamRegistrationDto.student",
					"Not possible to register exam, student's current year of study need to be less than year of subject");
		}
		
	}

}

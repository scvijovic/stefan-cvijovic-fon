package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import stefan.cvijovic.fon.myproject.model.SubjectDto;
import stefan.cvijovic.fon.myproject.service.SubjectService;

public class SubjectDtoFormatter implements Formatter<SubjectDto> {
	
	private final SubjectService subjectService;
	
	@Autowired
	public SubjectDtoFormatter(SubjectService subjectService) {
		super();
		this.subjectService = subjectService;
	}

	@Override
	public String print(SubjectDto object, Locale locale) {
		return object.getName();
	}

	@Override
	public SubjectDto parse(String text, Locale locale) throws ParseException {
		
		return subjectService.findById(text);
	}

}

package stefan.cvijovic.fon.myproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.cvijovic.fon.myproject.model.ExamDto;
import stefan.cvijovic.fon.myproject.model.ProfessorDto;
import stefan.cvijovic.fon.myproject.model.SubjectDto;
import stefan.cvijovic.fon.myproject.service.ExamService;
import stefan.cvijovic.fon.myproject.service.ProfessorService;
import stefan.cvijovic.fon.myproject.service.SubjectService;

@Controller
@SessionAttributes(value = "examDto")
public class ExamController {
	
	private final ExamService examService;
	private final ProfessorService professorService;
	private final SubjectService subjectService;
	
	@Autowired
	public ExamController(ExamService examService, ProfessorService professorService, SubjectService subjectService) {
		super();
		this.examService = examService;
		this.professorService = professorService;
		this.subjectService = subjectService;
	}
	
	@GetMapping(value = "exam/exam_add")
	public ModelAndView toPage() {
		ModelAndView modelAndView = new ModelAndView("exam/exam_add");
		return modelAndView;
	}
	
	
	@GetMapping(value = "exam/exam_all")
	public ModelAndView all(){
		ModelAndView modelAndView = new ModelAndView("exam/exam_all");
		return modelAndView;
	}
	
	@GetMapping(value = "exam/exam_remove")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/exam/exam_all");
		examService.deleteById(id);
		return modelAndView;
	}
	
	@PostMapping(value = "exam/exam_save")
	public ModelAndView save(@Valid @ModelAttribute("examDto") ExamDto examDto
			, Errors errors, SessionStatus sessionStatus ) {  
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(examDto);
		if(errors.hasErrors()) {
			modelAndView.setViewName("/exam/exam_add");
			modelAndView.addObject("examDto", examDto);
		} else {
			examService.save(examDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/exam/exam_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "exam/exam_edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("exam/exam_edit");
		modelAndView.addObject("examDto", examService.findById(id));
		return modelAndView;
	}
	
	@PostMapping(value = "exam/exam_update")
	public ModelAndView update(@Valid @ModelAttribute("examDto") ExamDto examDto,
			Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/exam/exam_edit");
			modelAndView.addObject("examDto", examDto);
		}
		else {
			examService.save(examDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/exam/exam_all");
		}
		return modelAndView;
	}
	
	
	@GetMapping(value = "exam/exam_details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("exam/exam_details");
		modelAndView.addObject("examDto", examService.findById(id));
		return modelAndView;
	}
	
	
	
	@ModelAttribute(name = "subjects")
	public List<SubjectDto> subjects(){
		return subjectService.getAll();
	}
	
	@ModelAttribute(name = "professors")
	public List<ProfessorDto> professors(){
		return professorService.getAll();
	}
	
	@ModelAttribute(name="examDto")
	public ExamDto examDto() {
		return new ExamDto();
	}
	
	@ModelAttribute(name="exams")
	public List<ExamDto> exams(){
		return examService.getAll();
	}
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	

}

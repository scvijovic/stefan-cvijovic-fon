package stefan.cvijovic.fon.myproject.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import stefan.cvijovic.fon.myproject.model.ProfessorDto;
import stefan.cvijovic.fon.myproject.service.ProfessorService;

public class ProfessorDtoValidator implements Validator {
	
	private final ProfessorService professorService;
	
	@Autowired
	public ProfessorDtoValidator(ProfessorService professorService) {
		super();
		this.professorService = professorService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return ProfessorDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProfessorDto professorDto = (ProfessorDto)target;
		
		if(professorDto.getFirstname()!=null) {
			if(professorDto.getFirstname().trim().length() < 3) {
				errors.rejectValue("firstname", "StudentDto.firstname",
						"Firstname must have more than 3 characters");
			}
		}
		
		if(professorDto.getLastname()!=null) {
			if(professorDto.getLastname().trim().length() < 3) {
				errors.rejectValue("lastname", "StudentDto.lastname",
						"Lastname must have more than 3 characters");
			}
		}
		
		if(professorDto.getEmail()!=null) {
			if(!professorDto.getEmail().contains("@")) {
				errors.rejectValue("email", "StudentDto.email", 
						"Email must contain @");
			}
		}
		
		if(professorDto.getAdress()!=null) {
			if(professorDto.getAdress().trim().length() < 3) {
				errors.rejectValue("adress", "StudentDto.adress",
						"Address must have more than 3 characters");
			}
		}
		
		if(professorDto.getPhone()!=null) {
			if(professorDto.getPhone().trim().length() < 6) {
				errors.rejectValue("phone", "StudentDto.phone",
						"Phone number must have more than 6 characters");
			}
		}
		
	}

}

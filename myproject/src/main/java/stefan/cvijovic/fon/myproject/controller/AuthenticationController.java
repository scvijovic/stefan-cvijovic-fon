package stefan.cvijovic.fon.myproject.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.cvijovic.fon.myproject.model.UserDto;
import stefan.cvijovic.fon.myproject.service.UserService;

@Controller
@RequestMapping(value = "")
public class AuthenticationController {
	
	private final UserService userService;

	@Autowired
	public AuthenticationController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	@GetMapping
	public ModelAndView homeApp() {
		System.out.println("============== homeApp() ===========================");
		ModelAndView modelAndView = new ModelAndView("authentication/login");
		return modelAndView;
	}
	
	@ModelAttribute(name = "userDto")
	public UserDto userDto() {
		return new UserDto();
	}
	
	@PostMapping(value = "authentication/login")
	public ModelAndView confirm(@Valid @ModelAttribute ("userDto") UserDto userDto, Errors errors, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		userService.findUserServ(userDto);
		if(errors.hasErrors()) {
			modelAndView.setViewName("/authentication/login");
			modelAndView.addObject("userDto", userDto);
		} else {
			
			HttpSession session = request.getSession(false);
			session.setAttribute("user", userDto);
			modelAndView.setViewName("redirect:/home");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "authentication/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("/authentication/login");
		modelAndView.addObject("userDto", new UserDto());
		return modelAndView;
	}
	
	@GetMapping(value = "authentication/logout")
	public ModelAndView logout(HttpServletRequest request, SessionStatus status) {
	    HttpSession session = request.getSession(false);
	    if (session != null) {
	        session.invalidate();
	        status.setComplete();
	    }
	    return new ModelAndView("redirect:/authentication/login");
	}
	
	
	
	

}

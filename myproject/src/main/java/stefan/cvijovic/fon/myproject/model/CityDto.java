package stefan.cvijovic.fon.myproject.model;

import java.io.Serializable;

public class CityDto implements Serializable {
	private static final long serialVersionUID = 6892409920833727397L;
	
	private Long id;
	private int number;
	private String name;
	
	public CityDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public CityDto(Long id, int number, String name) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityDto [id=" + id + ", number=" + number + ", name=" + name + "]";
	}

	
	
	
}

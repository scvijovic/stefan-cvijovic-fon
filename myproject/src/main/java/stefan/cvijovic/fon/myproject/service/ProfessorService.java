package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.ProfessorDto;

public interface ProfessorService {
		public void save(ProfessorDto professorDto);
		public List<ProfessorDto> getAll();
		public ProfessorDto findById(Long id);
		public void deleteById(Long id);
}

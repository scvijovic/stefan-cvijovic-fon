package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.UserDto;

public interface UserService {
	
	public List<UserDto> findUserServ(UserDto userDto)throws Exception;
}

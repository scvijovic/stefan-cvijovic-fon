package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.CityConverter;
import stefan.cvijovic.fon.myproject.entity.CityEntity;
import stefan.cvijovic.fon.myproject.model.CityDto;
import stefan.cvijovic.fon.myproject.repository.CityRepository;
import stefan.cvijovic.fon.myproject.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	
	private CityRepository cityRepository;
	private CityConverter cityConverter;
	
	
	@Autowired
	public CityServiceImpl(CityRepository cityRepository, CityConverter cityConverter) {
		super();
		this.cityRepository = cityRepository;
		this.cityConverter = cityConverter;
	}

	@Override
	public void save(CityDto cityDto) {
		cityRepository.save(cityConverter.dtoToEntity(cityDto));
		
	}

	@Override
	public List<CityDto> getAll() {
		List<CityEntity> entityList = cityRepository.findAll();
		List<CityDto> dtoList = cityConverter.entityToDtoList(entityList);
		return dtoList;
	}
	
	

	@Override
	public CityDto findById(Long id) {
		Optional<CityEntity> cityOptional = cityRepository.findById(id);
		if(cityOptional.isPresent()) {
			return cityConverter.entityToDto(cityOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		cityRepository.deleteById(id);
		
	}

}

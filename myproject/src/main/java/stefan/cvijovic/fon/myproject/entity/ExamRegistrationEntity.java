package stefan.cvijovic.fon.myproject.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "exam_registration")
public class ExamRegistrationEntity implements Serializable {
	
	private static final long serialVersionUID = 7L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private ExamEntity exam;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private StudentEntity student;
	
	public ExamRegistrationEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamRegistrationEntity(Long id, ExamEntity exam, StudentEntity student) {
		super();
		this.id = id;
		this.exam = exam;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExamEntity getExam() {
		return exam;
	}

	public void setExam(ExamEntity exam) {
		this.exam = exam;
	}

	public StudentEntity getStudent() {
		return student;
	}

	public void setStudent(StudentEntity student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "ExamRegistrationEntity [id=" + id + ", exam=" + exam + ", student=" + student + "]";
	}
	
	
	
	
	
	
	
	
	

}

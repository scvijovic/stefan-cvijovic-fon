package stefan.cvijovic.fon.myproject.controller;

import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.cvijovic.fon.myproject.model.CityDto;
import stefan.cvijovic.fon.myproject.model.StudentDto;
import stefan.cvijovic.fon.myproject.service.CityService;
import stefan.cvijovic.fon.myproject.service.StudentService;
import stefan.cvijovic.fon.myproject.validator.StudentDtoValidator;

@Controller
@SessionAttributes(value = "studentDto")
public class StudentController {
	
	private final CityService cityService;
	private final StudentService studentService;
	
	@Autowired
	public StudentController(CityService cityService, StudentService studentService) {
		super();
		this.cityService = cityService;
		this.studentService = studentService;
	}


	@GetMapping(value = "student/student_home")
	public String home() {
		System.out.println("====================================================================");
		System.out.println("====================   StudentController    ===================");
		System.out.println("====================================================================");
		return "student/student_home";
	}
	
	
	@GetMapping(value = "student/student_add")
	public ModelAndView toPage() {
		ModelAndView modelAndView = new ModelAndView("student/student_add");
		return modelAndView;
	}
	
	@GetMapping(value = "student/student_all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("student/student_all");
		return modelAndView;
	}
	
	@PostMapping(value = "student/student_save")
	public ModelAndView save(@Valid @ModelAttribute("studentDto") StudentDto studentDto
			, Errors errors, SessionStatus sessionStatus ) {  //
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(studentDto);
		if(errors.hasErrors()) {
			modelAndView.setViewName("/student/student_add");
			modelAndView.addObject("studentDto", studentDto);
		} else {
			studentService.save(studentDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/student/student_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "student/student_edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/student_edit");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}
	
	@PostMapping(value = "student/student_update")
	public ModelAndView update(@Valid @ModelAttribute("studentDto") StudentDto studentDto
			, Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("student/student_edit");
			modelAndView.addObject("studentDto", studentDto);
		} else {
			studentService.save(studentDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/student/student_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "student/student_remove")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/student/student_all");
		studentService.deleteById(id);;
		return modelAndView;
	}
	
	@GetMapping(value = "student/student_details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("student/student_details");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}
	
	/*@GetMapping(value = "student/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("student/add");
		return modelAndView;
	}*/
	
	
	@ModelAttribute(name = "studentDto")
	public StudentDto getStudentDto() {
		return new StudentDto();
	}
	
	@ModelAttribute(value = "students")
	public List<StudentDto> listStudents(){
		return studentService.getAll();
	}
	
	@ModelAttribute(value = "cities")
	public List<CityDto> listCities() {
		return cityService.getAll();
	}
	
	@InitBinder
	private void InitBinder(WebDataBinder binder) {
		binder.addValidators(new StudentDtoValidator(studentService));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

	}
	
	
}

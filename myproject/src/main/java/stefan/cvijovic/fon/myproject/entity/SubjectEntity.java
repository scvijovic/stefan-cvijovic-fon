package stefan.cvijovic.fon.myproject.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subject")
public class SubjectEntity implements Serializable {
	public static final long serialVersionUID = 14404022020L;
	@Id
	@Column(name = "SUBJECT_NAME", nullable = false, length = 30)
	private String name;
	@Column(length = 200)
	private String description;
	@Column(length = 1)
	private int yearOfStudy;
	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private Semester semester;
	
	public SubjectEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SubjectEntity(String name, String description, int yearOfStudy, Semester semester) {
		super();
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}
	
	
	
	
	
	
}

package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.SubjectConverter;
import stefan.cvijovic.fon.myproject.entity.SubjectEntity;
import stefan.cvijovic.fon.myproject.model.SubjectDto;
import stefan.cvijovic.fon.myproject.repository.SubjectRepository;
import stefan.cvijovic.fon.myproject.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {
	
	private SubjectRepository subjectRepository;
	private SubjectConverter subjectConverter;
	
	
	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository, SubjectConverter subjectConverter) {
		super();
		this.subjectRepository = subjectRepository;
		this.subjectConverter = subjectConverter;
	}

	@Override
	public void save(SubjectDto subjectDto) {
		subjectRepository.save(subjectConverter.dtoToEntity(subjectDto));
		
	}

	@Override
	public List<SubjectDto> getAll() {
		List<SubjectEntity> listSubjectEntity = subjectRepository.findAll();
		List<SubjectDto> listSubjectDto = subjectConverter.entityToDtoList(listSubjectEntity);
		return listSubjectDto;
	}

	@Override
	public SubjectDto findById(String id) {
		Optional<SubjectEntity> subjectOptional = subjectRepository.findById(id);
		if(subjectOptional.isPresent()) {
			return subjectConverter.entityToDto(subjectOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(String id) {
		subjectRepository.deleteById(id);
	}

}

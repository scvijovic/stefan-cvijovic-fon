package stefan.cvijovic.fon.myproject.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "exam")
public class ExamEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "SUBJECT_ID", referencedColumnName = "SUBJECT_NAME")
	private SubjectEntity subject;
	@ManyToOne
	@JoinColumn(name = "PROFESSOR_ID", referencedColumnName = "PROFESSOR_ID")
	private ProfessorEntity professor;
	@Temporal(TemporalType.DATE)
	private Date dateOfExam;
	
	public ExamEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamEntity(Long id, SubjectEntity subject, ProfessorEntity professor, Date dateOfExam) {
		super();
		this.id = id;
		this.subject = subject;
		this.professor = professor;
		this.dateOfExam = dateOfExam;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	public ProfessorEntity getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorEntity professor) {
		this.professor = professor;
	}

	public Date getDateOfExam() {
		return dateOfExam;
	}

	public void setDateOfExam(Date dateOfExam) {
		this.dateOfExam = dateOfExam;
	}
	
	
	
	

}

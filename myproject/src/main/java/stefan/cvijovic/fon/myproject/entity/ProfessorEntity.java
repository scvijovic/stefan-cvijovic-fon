package stefan.cvijovic.fon.myproject.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name = "professor")
public class ProfessorEntity implements Serializable {
	 public static final long serialVersionUID = 13004022020L;
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Column(name = "PROFESSOR_ID")
	 private Long id;
	 @Column(nullable = false, length = 30)
	 private String firstname;
	 @Column(nullable = false, length = 30)
	 private String lastname;
	 @Column(unique = true, length = 30)
	 private String email;
	 private String adress;
	 @ManyToOne
	 @JoinColumn(name = "CITY_ID")
	 private CityEntity city;
	 private String phone;
	 @Temporal(TemporalType.DATE)
	 @Column(nullable = false)
	 private Date reelectionDate;
	 @ManyToOne
	 @JoinColumn(name = "TITLE_ID", nullable = false)
	 private TitleEntity title;
	 
	public ProfessorEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ProfessorEntity(Long id, String firstname, String lastname, String email, String adress, CityEntity city,
			String phone, Date reelectionDate, TitleEntity title) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public CityEntity getCity() {
		return city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleEntity getTitle() {
		return title;
	}

	public void setTitle(TitleEntity title) {
		this.title = title;
	}
	
	
	
	
	
	
	 
	 
	 
	 

	
	
	 
	 
	 
}

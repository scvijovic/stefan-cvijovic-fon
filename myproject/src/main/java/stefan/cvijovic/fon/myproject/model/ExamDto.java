package stefan.cvijovic.fon.myproject.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import stefan.cvijovic.fon.myproject.entity.ProfessorEntity;
import stefan.cvijovic.fon.myproject.entity.SubjectEntity;

public class ExamDto implements Serializable {

	private static final long serialVersionUID = 5L;
	
	private Long id;
	@NotNull(message = "You must choose subject")
	private SubjectDto subject;
	@NotNull(message = "You must choose professor")
	private ProfessorDto professor;
	@NotNull(message = "You must choose date of exam")
	private Date dateOfExam;
	
	public ExamDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamDto(Long id, SubjectDto subject, ProfessorDto professor, Date dateOfExam) {
		super();
		this.id = id;
		this.subject = subject;
		this.professor = professor;
		this.dateOfExam = dateOfExam;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectDto getSubject() {
		return subject;
	}

	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}

	public ProfessorDto getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorDto professor) {
		this.professor = professor;
	}

	public Date getDateOfExam() {
		return dateOfExam;
	}

	public void setDateOfExam(Date dateOfExam) {
		this.dateOfExam = dateOfExam;
	}

	
	
	
	
	
	

}

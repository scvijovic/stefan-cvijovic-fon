package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.ExamConverter;
import stefan.cvijovic.fon.myproject.entity.ExamEntity;
import stefan.cvijovic.fon.myproject.model.ExamDto;
import stefan.cvijovic.fon.myproject.repository.ExamRepository;
import stefan.cvijovic.fon.myproject.service.ExamService;
@Service
@Transactional
public class ExamServiceImpl implements ExamService {
	
	private ExamRepository examRepository;
	private ExamConverter examConverter;
	
	
	@Autowired
	public ExamServiceImpl(ExamRepository examRepository, ExamConverter examConverter) {
		super();
		this.examRepository = examRepository;
		this.examConverter = examConverter;
	}

	@Override
	public void save(ExamDto examDto) {
		examRepository.save(examConverter.dtoToEntity(examDto));
		
	}

	@Override
	public List<ExamDto> getAll() {
		List<ExamEntity> listEntity = examRepository.findAll();
		List<ExamDto> listDto = examConverter.entityToDtoList(listEntity);
		return listDto;
	}

	@Override
	public ExamDto findById(Long id) {
		Optional<ExamEntity> examOptional = examRepository.findById(id);
		if(examOptional.isPresent()) {
			return examConverter.entityToDto(examOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		examRepository.deleteById(id);
		
	}

}

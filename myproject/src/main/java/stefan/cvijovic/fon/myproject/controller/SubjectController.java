package stefan.cvijovic.fon.myproject.controller;

import java.util.Arrays;
import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.cvijovic.fon.myproject.entity.Semester;
import stefan.cvijovic.fon.myproject.model.SubjectDto;
import stefan.cvijovic.fon.myproject.service.SubjectService;
import stefan.cvijovic.fon.myproject.validator.SubjectDtoValidator;

@Controller
@SessionAttributes(value = "subjectDto")
public class SubjectController {
	private final SubjectService subjectService;
	
	@Autowired
	public SubjectController(SubjectService subjectService) {
		super();
		this.subjectService = subjectService;
	}

	@GetMapping(value = "subject/subject_home")
	public String home() {
		System.out.println("====================================================================");
		System.out.println("====================   SubjectController    ===================");
		System.out.println("====================================================================");
		return "subject/subject_home";
	}
	
	@GetMapping(value = "subject/subject_add")
	public ModelAndView toPage() {
		ModelAndView modelAndView = new ModelAndView("subject/subject_add");
		return modelAndView;
	}
	
	
	/*@GetMapping(value = "subject/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("subject/add");
		return modelAndView;
	}
	*/
	
	@ModelAttribute(name = "subjectDto")
	public SubjectDto getSubjectDto() {
		return new SubjectDto();
	}
	
	@ModelAttribute(value = "subjects")
	public List<SubjectDto> subjectDtos(){
		return subjectService.getAll();
	}
	
	@ModelAttribute(value="semesters")
	public List<Semester> semesters()
	{
		return Arrays.asList(Semester.values());
	}
	
	@GetMapping(value = "subject/subject_all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("subject/subject_all");
		return modelAndView;
	}
	
	@GetMapping(value = "subject/subject_remove")
	public ModelAndView remove(@RequestParam("name") String name) {
		ModelAndView modelAndView = new ModelAndView("redirect:/subject/subject_all");
		subjectService.deleteById(name);
		return modelAndView;
	}
	
	@PostMapping(value = "subject/subject_save")
	public ModelAndView save(@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto,
			Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/subject/subject_add");
			modelAndView.addObject("subjectDto", subjectDto);
		} else {
			subjectService.save(subjectDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/subject/subject_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "subject/subject_edit")
	public ModelAndView edit(@RequestParam("name") String name) {
		ModelAndView modelAndView = new ModelAndView("/subject/subject_edit");
		modelAndView.addObject("subjectDto", subjectService.findById(name));
		return modelAndView;
	}
	
	@PostMapping(value = "subject/subject_update")
	public ModelAndView update(@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto,
			Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/subject/subject_edit");
			modelAndView.addObject("subjectDto", subjectDto);
		} else {
			subjectService.save(subjectDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/subject/subject_all");
		}
		return modelAndView;
		
	}
	
	@GetMapping(value = "subject/subject_details")
	public ModelAndView details(@RequestParam("name") String name) {
		ModelAndView modelAndView = new ModelAndView("subject/subject_details");
		modelAndView.addObject("subjectDto", subjectService.findById(name));
		return modelAndView;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new SubjectDtoValidator(subjectService));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	
	
	
	
	
}

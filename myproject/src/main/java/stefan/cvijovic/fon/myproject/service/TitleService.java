package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.TitleDto;

public interface TitleService {
		public void save(TitleDto titleDto);
		public List<TitleDto> getAll();
		public TitleDto findById(Long id);
		public void deleteById(Long id);
}

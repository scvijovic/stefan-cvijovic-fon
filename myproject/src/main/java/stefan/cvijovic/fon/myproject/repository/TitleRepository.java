package stefan.cvijovic.fon.myproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import stefan.cvijovic.fon.myproject.entity.TitleEntity;

public interface TitleRepository extends JpaRepository<TitleEntity, Long> {

}

package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.ExamRegistrationDto;

public interface ExamRegistrationService {
	public void save(ExamRegistrationDto examRegistrationDto);
	public List<ExamRegistrationDto> getAll();
	public ExamRegistrationDto findById(Long id);
	public void deleteById(Long id);
}

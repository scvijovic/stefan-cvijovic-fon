package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.SubjectDto;

public interface SubjectService {
		
		public void save(SubjectDto subjectDto);
		public List<SubjectDto> getAll();
		public SubjectDto findById(String id);
		public void deleteById(String id);
}

package stefan.cvijovic.fon.myproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping(value = "")
public class HomeController {
		
		@GetMapping(value = "home")
		public ModelAndView home() {
			System.out.println("============== HomeController: home() ===========================");
			ModelAndView modelAndView = new ModelAndView("home");
			return modelAndView;
		}
		/*
		@GetMapping
		public ModelAndView homeWithSlash() {
			System.out.println("============== HomeController: home() ===========================");
			ModelAndView modelAndView = new ModelAndView("home");
			return modelAndView;
		}*/
}

package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.CityEntity;
import stefan.cvijovic.fon.myproject.model.CityDto;

@Component
public class CityConverter {
	
		public CityDto entityToDto(CityEntity cityEntity) {
			if(cityEntity == null) {
				return null;
			}
			return new CityDto(cityEntity.getId() ,cityEntity.getNumber(), cityEntity.getName());
		}
		
		public CityEntity dtoToEntity(CityDto cityDto) {
			if(cityDto == null) {
				return null;
			}
			return new CityEntity(cityDto.getId() ,cityDto.getNumber(), cityDto.getName());
		}
		
		public List<CityEntity> dtoToEntityList(List<CityDto> dtoList) {
			List<CityEntity> entityList = new ArrayList<CityEntity>();
			for(CityDto cityDto: dtoList) {
				entityList.add(dtoToEntity(cityDto));
			}
			return entityList;
		}
		
		public List<CityDto> entityToDtoList(List<CityEntity> entityList) {
			List<CityDto> dtoList = new ArrayList<CityDto>();
			for(CityEntity cityEntity: entityList) {
				dtoList.add(entityToDto(cityEntity));
			}
			return dtoList;
		}
}

package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.model.ExamRegistrationDto;
import stefan.cvijovic.fon.myproject.service.ExamRegistrationService;
@Component
public class ExamRegistrationDtoFormatter implements Formatter<ExamRegistrationDto> {
	
	private final ExamRegistrationService examRegistrationService;
	
	@Autowired
	public ExamRegistrationDtoFormatter(ExamRegistrationService examRegistrationService) {
		super();
		this.examRegistrationService = examRegistrationService;
	}

	@Override
	public String print(ExamRegistrationDto examRegistrationDto, Locale locale) {
		return examRegistrationDto.getId().toString();
	}

	@Override
	public ExamRegistrationDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ExamRegistrationDto examRegistrationDto = examRegistrationService.findById(id);
		return examRegistrationDto;
	}

}

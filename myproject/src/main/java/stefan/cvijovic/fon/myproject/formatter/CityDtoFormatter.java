package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.model.CityDto;
import stefan.cvijovic.fon.myproject.service.CityService;
@Component
public class CityDtoFormatter implements Formatter<CityDto> {
	
	private final CityService cityService;
	
	@Autowired
	public CityDtoFormatter(CityService cityService) {
		super();
		this.cityService = cityService;
	}

	@Override
	public String print(CityDto cityDto, Locale locale) {
		return cityDto.getId().toString();
	}

	@Override
	public CityDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		CityDto cityDto = cityService.findById(id);
		return cityDto;
	}

}

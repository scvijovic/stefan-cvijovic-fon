package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.model.StudentDto;
import stefan.cvijovic.fon.myproject.service.StudentService;
@Component
public class StudentDtoFormatter implements Formatter<StudentDto> {
	
	private final StudentService studentService;
	
	
	//@Autowired
	public StudentDtoFormatter(StudentService studentService) {
		super();
		this.studentService = studentService;
	}

	@Override
	public String print(StudentDto studentDto, Locale locale) {
		return studentDto.getId().toString();
	}

	@Override
	public StudentDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		StudentDto studentDto = studentService.findById(id);
		return studentDto;
	}

}

package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.UserEntity;
import stefan.cvijovic.fon.myproject.model.UserDto;

@Component
public class UserConverter {
	
	public UserDto entityToDto(UserEntity userEntity) {
		return new UserDto(userEntity.getId(), userEntity.getUsername(), userEntity.getPassword()); 
	}
	
	public UserEntity dtoToEntity(UserDto userDto) {
		return new UserEntity(userDto.getId(), userDto.getUsername(), userDto.getPassword()); 
	}
	
	public List<UserEntity> dtoToEntityList(List<UserDto> dtoList) {
		List<UserEntity> entityList = new ArrayList<UserEntity>();
		for(UserDto userDto: dtoList) {
			entityList.add(dtoToEntity(userDto));
		}
		return entityList;
	}
	
	
	public List<UserDto> entityToDtoList(List<UserEntity> entityList) {
		List<UserDto> dtoList = new ArrayList<UserDto>();
		for(UserEntity userEntity: entityList) {
			dtoList.add(entityToDto(userEntity));
		}
		return dtoList;
	}
	
	
}

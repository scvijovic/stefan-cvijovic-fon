package stefan.cvijovic.fon.myproject.model;

import java.io.Serializable;

public class ExamRegistrationDto implements Serializable {

	private static final long serialVersionUID = 5L;

	private Long id;
	private ExamDto exam;
	private StudentDto student;

	public ExamRegistrationDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExamRegistrationDto(Long id, ExamDto exam, StudentDto student) {
		super();
		this.id = id;
		this.exam = exam;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExamDto getExam() {
		return exam;
	}

	public void setExam(ExamDto exam) {
		this.exam = exam;
	}

	public StudentDto getStudent() {
		return student;
	}

	public void setStudent(StudentDto student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "ExamRegistrationDto [id=" + id + ", exam=" + exam + ", student=" + student + "]";
	}
	
	

}

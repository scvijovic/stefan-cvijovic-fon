package stefan.cvijovic.fon.myproject.model;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class StudentDto implements Serializable {

	private static final long serialVersionUID = 83804032020L;
	private Long id;
	@NotNull(message = "You must enter index number")
	private String indexNumber;
	@NotEmpty(message = "You must enter first name")
	private String firstname;
	@NotEmpty(message = "You must enter last name")
	private String lastname;
	private String email;
	private String adress;
	private CityDto city;
	private String phone;
	@NotNull(message = "You must enter current year of study")
	private int currentYearOfStudy;
	
	public StudentDto() {
		//city = new CityDto();
	}

	public StudentDto(Long id, String indexNumber, String firstname, String lastname, String email, String adress,
			CityDto city, String phone, int currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}
	
	public String getFullname() {
		return firstname + " " + lastname;
	}

	@Override
	public String toString() {
		return "StudentDto [indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", email=" + email + ", adress=" + adress + ", city=" + city + ", phone=" + phone
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	
}

package stefan.cvijovic.fon.myproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import stefan.cvijovic.fon.myproject.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
	
	public List<UserEntity> findUserByUsernameAndPassword(String username, String password);
}

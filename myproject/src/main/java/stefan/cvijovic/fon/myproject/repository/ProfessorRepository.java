package stefan.cvijovic.fon.myproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import stefan.cvijovic.fon.myproject.entity.ProfessorEntity;

public interface ProfessorRepository extends JpaRepository<ProfessorEntity, Long> {

}

package stefan.cvijovic.fon.myproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.cvijovic.fon.myproject.model.CityDto;
import stefan.cvijovic.fon.myproject.model.ProfessorDto;
import stefan.cvijovic.fon.myproject.model.TitleDto;
import stefan.cvijovic.fon.myproject.service.CityService;
import stefan.cvijovic.fon.myproject.service.ProfessorService;
import stefan.cvijovic.fon.myproject.service.TitleService;
import stefan.cvijovic.fon.myproject.validator.ProfessorDtoValidator;

@Controller
@SessionAttributes(value = "professorDto")
public class ProfessorController {
	
	private final ProfessorService professorService;
	private final CityService cityService;
	private final TitleService titleService;
	
	@Autowired
	public ProfessorController(ProfessorService professorService, CityService cityService, TitleService titleService) {
		super();
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
	}

	@GetMapping(value = "professor/professor_home")
	public String home() {
		System.out.println("====================================================================");
		System.out.println("====================   ProfessorController    ===================");
		System.out.println("====================================================================");
		return "professor/professor_home";
	}
	
	@GetMapping(value = "professor/professor_add")
	public ModelAndView toPage() {
		ModelAndView modelAndView = new ModelAndView("professor/professor_add");
		return modelAndView;
	}
	
	@GetMapping(value = "professor/professor_all")
	public ModelAndView all(){
		ModelAndView modelAndView = new ModelAndView("professor/professor_all");
		return modelAndView;
	}
	
	@GetMapping(value = "professor/professor_remove")
	public ModelAndView remove(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/professor/professor_all");
		professorService.deleteById(id);
		return modelAndView;
	}
	
	@PostMapping(value = "professor/professor_save")
	public ModelAndView save(@Valid @ModelAttribute("professorDto") ProfessorDto professorDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/professor/professor_add");
			modelAndView.addObject("professorDto", professorDto);
		}
		else {
			professorService.save(professorDto);
			modelAndView.setViewName("redirect:/professor/professor_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "professor/professor_edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/professor_edit");
		modelAndView.addObject("professorDto", professorService.findById(id));
		return modelAndView;
	}
	
	@PostMapping(value = "professor/professor_update")
	public ModelAndView update(@Valid @ModelAttribute("professorDto") ProfessorDto professorDto,
			Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/professor/professor_edit");
			modelAndView.addObject("professorDto", professorDto);
		}
		else {
			professorService.save(professorDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/professor/professor_all");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "professor/professor_details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("professor/professor_details");
		modelAndView.addObject("professorDto", professorService.findById(id));
		return modelAndView;
	}
	
	/*@GetMapping(value = "professor/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("professor/add");
		return modelAndView;
	}*/
	
	@ModelAttribute(name = "professorDto")
	public ProfessorDto getProfessorDto() {
		return new ProfessorDto();
	}
	
	@ModelAttribute(name = "professors")
	public List<ProfessorDto> listProfessorDto(){
		return professorService.getAll();
	}
	
	@ModelAttribute(name = "cities")
	public List<CityDto> listCityDto(){
		return cityService.getAll();
	}
	
	@ModelAttribute(name = "titles")
	public List<TitleDto> listTitleDto(){
		return titleService.getAll();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProfessorDtoValidator(professorService));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
}

package stefan.cvijovic.fon.myproject.config;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import stefan.cvijovic.fon.myproject.formatter.CityDtoFormatter;
import stefan.cvijovic.fon.myproject.formatter.ExamDtoFormatter;
import stefan.cvijovic.fon.myproject.formatter.ProfessorDtoFormatter;
import stefan.cvijovic.fon.myproject.formatter.ReelectionDateFormatter;
import stefan.cvijovic.fon.myproject.formatter.StudentDtoFormatter;
import stefan.cvijovic.fon.myproject.formatter.SubjectDtoFormatter;
import stefan.cvijovic.fon.myproject.formatter.TitleDtoFormatter;
import stefan.cvijovic.fon.myproject.service.CityService;
import stefan.cvijovic.fon.myproject.service.ExamRegistrationService;
import stefan.cvijovic.fon.myproject.service.ExamService;
import stefan.cvijovic.fon.myproject.service.ProfessorService;
import stefan.cvijovic.fon.myproject.service.StudentService;
import stefan.cvijovic.fon.myproject.service.SubjectService;
import stefan.cvijovic.fon.myproject.service.TitleService;
@EnableWebMvc
@ComponentScan(basePackages = {
		"stefan.cvijovic.fon.myproject.controller",
		"stefan.cvijovic.fon.myproject.formatter"
})
@Configuration
public class MyWebAppContextConfig implements WebMvcConfigurer {
	
	private SubjectService subjectService;
	private CityService cityService;
	private StudentService studentService;
	private TitleService titleService;
	private ProfessorService professorService;
	private ExamService examService;
	private ExamRegistrationService examRegistrationService;
	
	@Autowired
	public MyWebAppContextConfig(SubjectService subjectService, CityService cityService,
			StudentService studentService, TitleService titleService, ProfessorService professorService,
			ExamService examService, ExamRegistrationService examRegistrationService) {
		System.out.println("=================================================================");
		System.out.println("==================== MyWebContextConfig =========================");
		System.out.println("=================================================================");
		this.subjectService = subjectService;
		this.cityService = cityService;
		this.studentService = studentService;
		this.titleService = titleService;
		this.professorService = professorService;
		this.examService = examService;
		this.examRegistrationService = examRegistrationService;
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
	
	
	@Bean
	public ViewResolver tilesViewResolver() {
		TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setOrder(0);
		return tilesViewResolver;
	}
	
	
	@Bean
	public TilesConfigurer tilesCongigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(
				new String[] {"/WEB-INF/views/tiles/tiles.xml"}
		);
		return tilesConfigurer;
	}
	
	/*@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
	}*/
	
	/*@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("redirect:/pages/home");
	}*/
	
	@Bean(name = "simpleDateFormat")
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new StudentDtoFormatter(studentService));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		registry.addFormatter(new ProfessorDtoFormatter(professorService));
		registry.addFormatter(new ExamDtoFormatter(examService));
		registry.addFormatter(new ReelectionDateFormatter(simpleDateFormat()));
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}

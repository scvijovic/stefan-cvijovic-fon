package stefan.cvijovic.fon.myproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import stefan.cvijovic.fon.myproject.entity.SubjectEntity;
@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, String> {
		
}

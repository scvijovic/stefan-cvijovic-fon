package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.ProfessorEntity;
import stefan.cvijovic.fon.myproject.model.ProfessorDto;

@Component
public class ProfessorConverter {
	private final CityConverter cityConverter;
	private final TitleConverter titleConverter;
	
	@Autowired
	public ProfessorConverter(CityConverter cityConverter, TitleConverter titleConverter) {
		super();
		this.cityConverter = cityConverter;
		this.titleConverter = titleConverter;
	}
	
	public ProfessorDto entityToDto(ProfessorEntity professorEntity) {
		return new ProfessorDto(professorEntity.getId(), professorEntity.getFirstname(),
				professorEntity.getLastname(), professorEntity.getEmail()
				, professorEntity.getAdress(), cityConverter.entityToDto(professorEntity.getCity()),
				professorEntity.getPhone(), professorEntity.getReelectionDate(),
				titleConverter.entityToDto(professorEntity.getTitle()));
	}
	
	public ProfessorEntity dtoToEntity(ProfessorDto professorDto) {
		return new ProfessorEntity(professorDto.getId(), professorDto.getFirstname(), 
				professorDto.getLastname(), professorDto.getEmail(), 
				professorDto.getAdress(), cityConverter.dtoToEntity(professorDto.getCity()),
				professorDto.getPhone(), professorDto.getReelectionDate(),
				titleConverter.dtoToEntity(professorDto.getTitle()));
	}
	
	public List<ProfessorEntity> dtoToEntityList(List<ProfessorDto> dtoList) {
		List<ProfessorEntity> entityList = new ArrayList<ProfessorEntity>();
		for(ProfessorDto professorDto: dtoList) {
			entityList.add(dtoToEntity(professorDto));
		}
		return entityList;
	}
	
	public List<ProfessorDto> entityToDtoList(List<ProfessorEntity> entityList){
		List<ProfessorDto> dtoList = new ArrayList<ProfessorDto>();
		for(ProfessorEntity professorEntity: entityList) {
			dtoList.add(entityToDto(professorEntity));
		}
		return dtoList;
	}
	
	
	
	
	

}

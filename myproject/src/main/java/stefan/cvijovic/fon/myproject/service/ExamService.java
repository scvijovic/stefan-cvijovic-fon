package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.ExamDto;


public interface ExamService {
	public void save(ExamDto examDto);
	public List<ExamDto> getAll();
	public ExamDto findById(Long id);
	public void deleteById(Long id);
}

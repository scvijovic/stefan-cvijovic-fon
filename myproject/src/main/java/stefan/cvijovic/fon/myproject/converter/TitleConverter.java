package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.TitleEntity;
import stefan.cvijovic.fon.myproject.model.TitleDto;

@Component
public class TitleConverter {
		public TitleDto entityToDto(TitleEntity titleEntity) {
			return new TitleDto(titleEntity.getId(), titleEntity.getName());
		}
		
		public TitleEntity dtoToEntity(TitleDto titleDto) {
			return new TitleEntity(titleDto.getId(), titleDto.getName());
		}
		
		public List<TitleEntity> dtoToEntityList(List<TitleDto> dtoList){
			List<TitleEntity> entityList = new ArrayList<TitleEntity>();
			for(TitleDto titleDto: dtoList) {
				entityList.add(dtoToEntity(titleDto));
			}
			return entityList;
		}
		
		public List<TitleDto> entityToDtoList(List<TitleEntity> entityList){
			List<TitleDto> dtoList = new ArrayList<TitleDto>();
			for(TitleEntity titleEntity: entityList) {
				dtoList.add(entityToDto(titleEntity));
			}
			return dtoList;
		}
}

package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.ExamEntity;
import stefan.cvijovic.fon.myproject.model.ExamDto;

@Component
public class ExamConverter {
	private final ProfessorConverter professorConverter;
	private final SubjectConverter subjectConverter;
	
	@Autowired
	public ExamConverter( @Lazy ProfessorConverter professorConverter, @Lazy SubjectConverter subjectConverter) {
		super();
		this.professorConverter = professorConverter;
		this.subjectConverter = subjectConverter;
	}
	
	public ExamDto entityToDto(ExamEntity examEntity) {
		return new ExamDto(examEntity.getId(), subjectConverter.entityToDto(examEntity.getSubject()),
				professorConverter.entityToDto(examEntity.getProfessor()), examEntity.getDateOfExam());
	}
	
	public ExamEntity dtoToEntity(ExamDto examDto) {
		return new ExamEntity(examDto.getId(), subjectConverter.dtoToEntity(examDto.getSubject()),
				professorConverter.dtoToEntity(examDto.getProfessor()), examDto.getDateOfExam());
	}
	
	public List<ExamEntity> dtoToEntityList(List<ExamDto> dtoList) {
		List<ExamEntity> entityList = new ArrayList<ExamEntity>();
		for(ExamDto examDto : dtoList) {
			entityList.add(dtoToEntity(examDto));
		}
		return entityList;
	}
	
	
	public List<ExamDto> entityToDtoList(List<ExamEntity> entityList) {
		List<ExamDto> dtolist = new ArrayList<ExamDto>();
		for(ExamEntity examEntity: entityList) {
			dtolist.add(entityToDto(examEntity));
		}
		return dtolist;
	}
	
	
	
	
		
}

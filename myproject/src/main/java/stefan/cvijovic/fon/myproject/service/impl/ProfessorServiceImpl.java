package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.ProfessorConverter;
import stefan.cvijovic.fon.myproject.entity.ProfessorEntity;
import stefan.cvijovic.fon.myproject.model.ProfessorDto;
import stefan.cvijovic.fon.myproject.repository.ProfessorRepository;
import stefan.cvijovic.fon.myproject.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {
	
	private ProfessorRepository professorRepository;
	private ProfessorConverter professorConverter;
	
	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository, ProfessorConverter professorConverter) {
		super();
		this.professorRepository = professorRepository;
		this.professorConverter = professorConverter;
	}

	@Override
	public void save(ProfessorDto professorDto) {
		professorRepository.save(professorConverter.dtoToEntity(professorDto));
		
	}

	@Override
	public List<ProfessorDto> getAll() {
		List<ProfessorEntity> listEntity = professorRepository.findAll();
		List<ProfessorDto> listDto = professorConverter.entityToDtoList(listEntity);
		return listDto;
	}

	@Override
	public ProfessorDto findById(Long id) {
		Optional<ProfessorEntity> professorOptional = professorRepository.findById(id);
		if(professorOptional.isPresent()) {
			return professorConverter.entityToDto(professorOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		professorRepository.deleteById(id);
		
	}

}

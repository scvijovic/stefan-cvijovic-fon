package stefan.cvijovic.fon.myproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.model.ExamDto;
import stefan.cvijovic.fon.myproject.service.ExamService;

@Component
public class ExamDtoFormatter implements Formatter<ExamDto> {
	
	private final ExamService examService;
	
	@Autowired
	public ExamDtoFormatter(ExamService examService) {
		super();
		this.examService = examService;
	}

	@Override
	public String print(ExamDto examDto, Locale locale) {
		return examDto.getId().toString();
	}

	@Override
	public ExamDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ExamDto examDto = examService.findById(id);
		return examDto;
	}

}

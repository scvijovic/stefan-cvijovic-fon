package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.SubjectEntity;
import stefan.cvijovic.fon.myproject.model.SubjectDto;

@Component
public class SubjectConverter {
	
	public SubjectDto entityToDto(SubjectEntity subjectEntity) {
		return new SubjectDto(subjectEntity.getName(), subjectEntity.getDescription(),
				subjectEntity.getYearOfStudy(), subjectEntity.getSemester());
	}
	
	public SubjectEntity dtoToEntity(SubjectDto subjectDto) {
		return new SubjectEntity(subjectDto.getName(), subjectDto.getDescription(),
				subjectDto.getYearOfStudy(), subjectDto.getSemester());
	}
	
	public List<SubjectDto> entityToDtoList(List<SubjectEntity> entityList) {
		List<SubjectDto> dtoList = new ArrayList<SubjectDto>();
		for(SubjectEntity subjectEntity: entityList) {
			dtoList.add(entityToDto(subjectEntity));
		}
		return dtoList;
	}
	
	public List<SubjectEntity> dtoToEntityList(List<SubjectDto> dtoList) {
		List<SubjectEntity> entityList = new ArrayList<SubjectEntity>();
		for(SubjectDto subjectDto: dtoList) {
			entityList.add(dtoToEntity(subjectDto));
		}
		return entityList;
	}
}

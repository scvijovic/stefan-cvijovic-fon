package stefan.cvijovic.fon.myproject.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class StudentEntity implements Serializable {
	public static final long serialVersionUID = 11104022020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true, nullable = false, length = 10)
	private String indexNumber;
	@Column(nullable = false, length = 30)
	private String firstname;
	@Column(nullable = false, length = 30)
	private String lastname;
	@Column(unique = true, length = 30)
	private String email;
	private String adress;
	@ManyToOne
	@JoinColumn(name = "CITY_ID")
	private CityEntity city;
	private String phone;
	@Column(nullable = false, length = 7)
	private int currentYearOfStudy;
	
	public StudentEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentEntity(Long id, String indexNumber, String firstname, String lastname, String email, String adress,
			CityEntity city, String phone, int currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public CityEntity getCity() {
		return city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	@Override
	public String toString() {
		return "StudentEntity [indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", email=" + email + ", adress=" + adress + ", city=" + city + ", phone=" + phone
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	
	
	
	
}

package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.ExamRegistrationConverter;
import stefan.cvijovic.fon.myproject.entity.ExamRegistrationEntity;
import stefan.cvijovic.fon.myproject.model.ExamRegistrationDto;
import stefan.cvijovic.fon.myproject.repository.ExamRegistrationRepository;
import stefan.cvijovic.fon.myproject.service.ExamRegistrationService;
@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService {
	
	private ExamRegistrationRepository examRegistrationRepository;
	private ExamRegistrationConverter examRegistrationConverter;
	
	
	@Autowired
	public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository,
			ExamRegistrationConverter examRegistrationConverter) {
		super();
		this.examRegistrationRepository = examRegistrationRepository;
		this.examRegistrationConverter = examRegistrationConverter;
	}

	@Override
	public void save(ExamRegistrationDto examRegistrationDto) {
		examRegistrationRepository.save(examRegistrationConverter.dtoToEntity(examRegistrationDto));
		
	}

	@Override
	public List<ExamRegistrationDto> getAll() {
		List<ExamRegistrationEntity> listEntity = examRegistrationRepository.findAll();
		List<ExamRegistrationDto> listDto = examRegistrationConverter.entityToDtoList(listEntity);
		return listDto;
	}

	@Override
	public ExamRegistrationDto findById(Long id) {
		Optional<ExamRegistrationEntity> examRegistrationOpt = examRegistrationRepository.findById(id);
		if(examRegistrationOpt.isPresent()) {
			return examRegistrationConverter.entityToDto(examRegistrationOpt.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		examRegistrationRepository.deleteById(id);
		
	}

}

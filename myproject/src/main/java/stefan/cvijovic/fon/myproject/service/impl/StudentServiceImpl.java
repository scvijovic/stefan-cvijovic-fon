package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.StudentConverter;
import stefan.cvijovic.fon.myproject.entity.StudentEntity;
import stefan.cvijovic.fon.myproject.model.StudentDto;
import stefan.cvijovic.fon.myproject.repository.StudentRepository;
import stefan.cvijovic.fon.myproject.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	
	private StudentRepository studentRepository;
	private StudentConverter studentConverter;
	
	
	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository, StudentConverter studentConverter) {
		super();
		this.studentRepository = studentRepository;
		this.studentConverter = studentConverter;
	}

	@Override
	public void save(StudentDto studentDto) {
		StudentEntity studentEntity = studentConverter.dtoToEntity(studentDto);
		System.out.println(studentEntity);
		studentRepository.save(studentEntity);
		
	}

	@Override
	public List<StudentDto> getAll() {
		List<StudentEntity> listEntity = studentRepository.findAll();
		List<StudentDto> listDto = studentConverter.entityToDtoList(listEntity);
		return listDto;
	}

	@Override
	public StudentDto findById(Long id) {
		Optional<StudentEntity> studentOptional = studentRepository.findById(id);
		if(studentOptional.isPresent()) {
			return studentConverter.entityToDto(studentOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void deleteById(Long id) {
		studentRepository.deleteById(id);
		
	}

}

package stefan.cvijovic.fon.myproject.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import stefan.cvijovic.fon.myproject.model.SubjectDto;
import stefan.cvijovic.fon.myproject.service.SubjectService;

public class SubjectDtoValidator implements Validator {
	
	private final SubjectService subjectService;
	
	@Autowired
	public SubjectDtoValidator(SubjectService subjectService) {
		super();
		this.subjectService = subjectService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return SubjectDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SubjectDto subjectDto = (SubjectDto) target;
		
		if(subjectDto.getName()!=null) {
			if(subjectDto.getName().trim().length() < 3) {
				errors.rejectValue("name", "SubjectDto.name",
						"Subject name must have more than 3 characters");
			}
		}
		
	}
		
}

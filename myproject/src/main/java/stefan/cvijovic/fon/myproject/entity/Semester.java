package stefan.cvijovic.fon.myproject.entity;

public enum Semester {
		SUMMER("SUMMER"), WINTER("WINTER");
		private String name;

		private Semester(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		
	
}

package stefan.cvijovic.fon.myproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import stefan.cvijovic.fon.myproject.entity.StudentEntity;
import stefan.cvijovic.fon.myproject.model.StudentDto;

@Component
public class StudentConverter {
	
		private final CityConverter cityConverter;
		
		@Autowired
		public StudentConverter(CityConverter cityConverter) {
			this.cityConverter = cityConverter;
		}
		
		public StudentDto entityToDto(StudentEntity studentEntity) {
			return new StudentDto(studentEntity.getId(), studentEntity.getIndexNumber(), studentEntity.getFirstname(),
					studentEntity.getLastname(), studentEntity.getEmail(),
					studentEntity.getAdress(), cityConverter.entityToDto(studentEntity.getCity()),
					studentEntity.getPhone(), studentEntity.getCurrentYearOfStudy());
		}
		
		public StudentEntity dtoToEntity(StudentDto studentDto) {
			return new StudentEntity(studentDto.getId() ,studentDto.getIndexNumber(), studentDto.getFirstname(),
					studentDto.getLastname(), studentDto.getEmail(), studentDto.getAdress(),
					cityConverter.dtoToEntity(studentDto.getCity()),
					studentDto.getPhone(), studentDto.getCurrentYearOfStudy());
		}
		
		public List<StudentEntity> dtoToEntityList(List<StudentDto> dtoList){
			List<StudentEntity> entityList = new ArrayList<StudentEntity>();
			for(StudentDto studentDto: dtoList) {
				entityList.add(dtoToEntity(studentDto));
			}
			return entityList;
		}
		
		public List<StudentDto> entityToDtoList(List<StudentEntity> entityList){
			List<StudentDto> dtoList = new ArrayList<StudentDto>();
			for(StudentEntity studentEntity: entityList) {
				dtoList.add(entityToDto(studentEntity));
			}
			return dtoList;
		}
		
		
}

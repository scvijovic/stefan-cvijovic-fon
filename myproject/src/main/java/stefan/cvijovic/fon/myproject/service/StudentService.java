package stefan.cvijovic.fon.myproject.service;

import java.util.List;

import stefan.cvijovic.fon.myproject.model.StudentDto;

public interface StudentService {
		public void save(StudentDto studentDto);
		public List<StudentDto> getAll();
		public StudentDto findById(Long id);
		public void deleteById(Long id);
}

package stefan.cvijovic.fon.myproject.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.cvijovic.fon.myproject.converter.UserConverter;
import stefan.cvijovic.fon.myproject.entity.UserEntity;
import stefan.cvijovic.fon.myproject.model.UserDto;
import stefan.cvijovic.fon.myproject.repository.UserRepository;
import stefan.cvijovic.fon.myproject.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	
	private final UserRepository userRepository;
	private final UserConverter userConverter;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
		super();
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}



	@Override
	public List<UserDto> findUserServ(UserDto userDto) throws Exception {
		List<UserEntity> listEntity = userRepository.findUserByUsernameAndPassword(userDto.getUsername(), userDto.getPassword());
		List<UserDto> listDto = userConverter.entityToDtoList(listEntity);
		if(listDto.isEmpty()) {
			throw new Exception("Wrong username or password");
		}
		return listDto;
	}

}
